CREATE DATABASE if not exists baseInstrumentos;
--
USE baseInstrumentos;
--
CREATE TABLE if not exists marca (
id int auto_increment primary key,
nombremarca varchar(30),
pais varchar(20),
delegacion varchar(30),
web varchar(50)
);
--
create table if not exists material(
id int auto_increment primary key,
nombreMaterial varchar(30),
calidad varchar(20),
organico varchar(2),
procedenciaa varchar(30)
);
--
CREATE TABLE if not exists instrumentos(
idinstrumento int auto_increment primary key,
codigo varchar(10) not null unique,
tipo varchar(15),
idmarca int,
nombre varchar(50),
pvp double,
fecha_adquisicion date,
marcaInstrumento varchar(30),
material varchar(30),
idMaterial int,
FOREIGN KEY (idMarca) REFERENCES marca (id),
FOREIGN KEY (idMaterial) REFERENCES material (id)
);
--
delimiter ||
create function existeCodigo(f_code varchar(10))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idInstrumento) from instrumentos)) do
    if  ((select codigo from instrumentos where  idInstrumento= (i + 1)) like f_code) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeInstrumento(f_name varchar(50))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idInstrumento) from instrumentos)) do
    if  ((select nombre from instrumentos where  idInstrumento= (i + 1)) like f_name) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeMaterial(f_name varchar(30))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(id) from material)) do
    if  ((select nombreMaterial from material where id = (i + 1)) like f_name) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeMarca(f_name varchar(62))
    returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(id) from marca)) do
    if  ((select concat(delegacion, ', ', web) from marca where id = (i + 1)) like f_name) then return 1;
end if;
    set i = i + 1;
end while;
return 0;
end; ||
delimiter ;