package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    private Temas temas;
    boolean refrescar;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        this.temas = temas;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void refrescarTodo() {
        refrescarMaterial();
        refrescarMarca();
        refrescarInstrumentos();
        refrescar = false;
    }

    private void addActionListeners(ActionListener listener) {
        //Listener de instrumentos
        vista.addInstrumentobtn.addActionListener(listener);
        vista.addInstrumentobtn.setActionCommand("addInstrumentobtn");
        vista.eliminarInstrumentoBtn.addActionListener(listener);
        vista.eliminarInstrumentoBtn.setActionCommand("eliminarInstrumentoBtn");
        vista.borrarTodosInstrumentosBtn.addActionListener(listener);
        vista.modificarInstrumentosBtn.addActionListener(listener);
        vista.modificarInstrumentosBtn.setActionCommand("modificarInstrumentosBtn");
        //vista.buscarPorCódigoButton.addActionListener(listener);
        //vista.buscarPorNombreButton.addActionListener(listener);

        //Listener de materiales
        vista.addMaterialBtn.addActionListener(listener);
        vista.addMaterialBtn.setActionCommand("addMaterialBtn");
        vista.eliminarMaterialBtn.addActionListener(listener);
        vista.eliminarMaterialBtn.setActionCommand("eliminarMaterialBtn");
        vista.modificarMaterialBtn.addActionListener(listener);
        vista.modificarMaterialBtn.setActionCommand("modificarMaterialBtn");
        vista.rbtnNo.addActionListener(listener);
        vista.rbtnSi.addActionListener(listener);

        //Listener de marcas
        vista.addMarcaBtn.addActionListener(listener);
        vista.addMarcaBtn.setActionCommand("addMarcaBtn");
        vista.eliminarMarcaBtn.addActionListener(listener);
        vista.eliminarMarcaBtn.setActionCommand("eliminarMarcaBtn");
        vista.modificarMarcaBtn.addActionListener(listener);
        vista.modificarMarcaBtn.setActionCommand("modificarMarcaBtn");

        vista.itemOpciones.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemParametros.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);

        vista.temas.btnConfirmar.addActionListener(listener);


    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addItemListeners(Controlador controlador) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tablaMaterial.getSelectionModel())) {
                int row = vista.tablaMaterial.getSelectedRow();
                vista.txtNombreMaterial.setText(String.valueOf(vista.tablaMaterial.getValueAt(row, 1)));
                vista.comboCalidad.setSelectedItem(String.valueOf(vista.tablaMaterial.getValueAt(row, 2)));
                if (vista.getOrganico() == "Si") {
                    vista.rbtnSi.setSelected(Boolean.parseBoolean(String.valueOf(vista.tablaMaterial.getValueAt(row, 3))));
                }
                if (vista.getOrganico() == "No") {
                    vista.rbtnNo.setSelected(Boolean.parseBoolean(String.valueOf(vista.tablaMaterial.getValueAt(row, 3))));
                }
                vista.txtProcedencia.setText(String.valueOf(vista.tablaMaterial.getValueAt(row, 4)));
            } else if (e.getSource().equals(vista.tablaMarca.getSelectionModel())) {
                int row = vista.tablaMarca.getSelectedRow();
                vista.txtNombreMarca.setText(String.valueOf(vista.tablaMarca.getValueAt(row, 1)));
                vista.comboPais.setSelectedItem(String.valueOf(vista.tablaMarca.getValueAt(row, 2)));
                vista.txtDelegacion.setText(String.valueOf(vista.tablaMarca.getValueAt(row, 3)));
                vista.txtWeb.setText(String.valueOf(vista.tablaMarca.getValueAt(row, 4)));
            } else if (e.getSource().equals(vista.tablaInstrumentos.getSelectionModel())) {
                int row = vista.tablaInstrumentos.getSelectedRow();
                vista.txtCodigo.setText(String.valueOf(vista.tablaInstrumentos.getValueAt(row, 1)));
                vista.comboTipo.setSelectedItem(String.valueOf(vista.tablaInstrumentos.getValueAt(row, 2)));
                vista.txtNombre.setText(String.valueOf(vista.tablaInstrumentos.getValueAt(row, 3)));
                vista.txtPvp.setText(String.valueOf(vista.tablaInstrumentos.getValueAt(row, 4)));
                vista.datePicker.setDate(Date.valueOf(String.valueOf(vista.tablaInstrumentos.getValueAt(row, 5))).toLocalDate());
                vista.comboMarca.setSelectedItem(String.valueOf(vista.tablaInstrumentos.getValueAt(row, 6)));
                vista.comboMaterial.setSelectedItem(String.valueOf(vista.tablaInstrumentos.getValueAt(row, 7)));
            } else if (e.getValueIsAdjusting()
                    && (((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar)) {

                if (e.getSource().equals(vista.tablaMaterial.getSelectionModel())) {
                    borrarCamposMaterial();
                } else if (e.getSource().equals(vista.tablaMarca.getSelectionModel())) {
                    borrarCamposMarca();
                } else if (e.getSource().equals(vista.tablaInstrumentos.getSelectionModel())) {
                    borrarCamposInstrumentos();
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {

            case "Opciones":
                vista.temas.setVisible(true);

                break;
            case "Confirmar":
                if (vista.temas.rbtnLight.isSelected()) {
                    vista.color1 = new Color(255, 255, 255);
                    vista.color2 = new Color(255, 255, 255);
                    vista.color3 = new Color(255, 255, 255);
                    vista.color4 = new Color(0, 0, 0);
                    vista.color5 = new Color(255, 255, 255);

                    vista.setColors1(vista.color1);
                    vista.setColor3(vista.color3);
                    vista.setColor2(vista.color2);
                    vista.setColor4(vista.color4);
                    vista.setColor5(vista.color5);
                    vista.setColor6(Color.BLACK);

                    vista.temas.dispose();
                    vista.temas.dispose();
                }

                if (vista.temas.rbtnDark.isSelected()) {
                    vista.color1 = new Color(0, 0, 0);
                    vista.color2 = new Color(0, 0, 0);
                    vista.color3 = new Color(0, 0, 0);
                    vista.color4 = new Color(255, 255, 255);
                    vista.color5 = new Color(0, 0, 0);

                    vista.setColors1(vista.color1);
                    vista.setColor3(vista.color3);
                    vista.setColor2(vista.color2);
                    vista.setColor4(vista.color4);
                    vista.setColor5(vista.color5);
                    vista.setColor6(Color.WHITE);

                    vista.temas.dispose();
                }

                if (vista.temas.rbtnMainTheme.isSelected()) {
                    vista.color1 = new Color(170, 107, 57);
                    vista.color2 = new Color(128, 68, 21);
                    vista.color3 = new Color(212, 153, 106);
                    vista.color4 = new Color(255, 255, 255);
                    vista.color5 = new Color(255, 208, 170);
                    vista.setColors1(vista.color1);
                    vista.setColor3(vista.color3);
                    vista.setColor2(vista.color2);
                    vista.setColor4(vista.color4);
                    vista.setColor5(vista.color5);
                    vista.setColor6(Color.BLACK);
                    vista.temas.dispose();
                }
                break;

            case "Salir":
                System.exit(0);
                break;

            case "Parámetros":
                vista.adminPasswordDialog.setVisible(true);
                break;

            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
                break;

            case "Guardar":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(),
                        vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()),
                        String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());
                break;


            case "addInstrumentobtn":
                try {
                    if (comprobarInstrumentosLimpio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaInstrumentos.clearSelection();
                    } else if (modelo.codigoYaExiste(vista.txtCodigo.getText())) {
                        Util.showErrorAlert("Ese código ya existe");
                        vista.tablaInstrumentos.clearSelection();
                    } else if (modelo.instrumentoYaExiste(vista.txtNombre.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe");
                        vista.tablaInstrumentos.clearSelection();
                    } else {
                        modelo.insertarInstrumento(
                                vista.txtCodigo.getText(),
                                String.valueOf(vista.comboTipo.getSelectedItem()),
                                vista.txtNombre.getText(),
                                Double.parseDouble(vista.txtPvp.getText()),
                                vista.datePicker.getDate(),
                                String.valueOf(vista.comboMarca.getSelectedItem()),
                                String.valueOf(vista.comboMaterial.getSelectedItem()));
                    }
                } catch (NumberFormatException e1) {
                    Util.showErrorAlert("Introduce números en precio");
                }
                borrarCamposInstrumentos();
                refrescarInstrumentos();
                break;

            case "addMarcaBtn":

                if (comprobarMarcaLimpio()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    vista.tablaMarca.clearSelection();
                } else if (modelo.marcaYaExiste(vista.txtNombreMarca.getText())) {
                    Util.showErrorAlert("Ese nombre de marca ya existe");
                    vista.tablaMarca.clearSelection();
                } else {
                    modelo.insertarMarca(
                            vista.txtNombreMarca.getText(),
                            String.valueOf(vista.comboPais.getSelectedItem()),
                            vista.txtDelegacion.getText(),
                            vista.txtWeb.getText());
                }

                borrarCamposMarca();
                refrescarMarca();
                break;

            case "addMaterialBtn":
                if (vista.rbtnSi.isSelected()) {
                    vista.rbtnNo.setSelected(false);
                    vista.setOrganico("Si");
                }
                if (vista.rbtnNo.isSelected()) {
                    vista.rbtnSi.setSelected(false);
                    vista.setOrganico("No");
                }

                if (comprobarMaterialLimpio()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    vista.tablaMaterial.clearSelection();
                } else if (modelo.materialYaExiste(vista.txtNombreMaterial.getText())) {
                    Util.showErrorAlert("Ese nombre de material ya existe");
                    vista.tablaMaterial.clearSelection();
                } else {

                    modelo.insertarMaterial(
                            vista.txtNombreMaterial.getText(),
                            String.valueOf(vista.comboCalidad.getSelectedItem()),
                            vista.getOrganico(),
                            vista.txtWeb.getText());

                }
                borrarCamposMaterial();
                refrescarMaterial();
                break;

            case "eliminarInstrumentoBtn":
                modelo.borrarInstrumento(Integer.parseInt((String) vista.tablaInstrumentos.getValueAt(vista.tablaInstrumentos.getSelectedRow(), 0)));
                borrarCamposInstrumentos();
                refrescarInstrumentos();
                break;

            case "modificarInstrumentosBtn":
                try {
                    if (comprobarInstrumentosLimpio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaInstrumentos.clearSelection();
                    } else {
                        modelo.modificarInstrumento(
                                vista.txtCodigo.getText(),
                                String.valueOf(vista.comboTipo.getSelectedItem()),
                                vista.txtNombre.getText(),
                                Double.parseDouble(vista.txtPvp.getText()),
                                vista.datePicker.getDate(),
                                String.valueOf(vista.comboMarca.getSelectedItem()),
                                String.valueOf(vista.comboMaterial.getSelectedItem()));
                    }
                } catch (NumberFormatException e1) {
                    Util.showErrorAlert("Introduce números en precio");
                    vista.tablaInstrumentos.clearSelection();
                }
                borrarCamposInstrumentos();
                refrescarInstrumentos();
                break;

            case "Borrar todo":
                int resp = Util.confirmMessage("¿Estás seguro?\nEsto borrará todos los instrumentos de la BBDD",
                        "Borrar todos los instrumentos");
                if (resp == JOptionPane.OK_OPTION) {
                    modelo.killemall();
                    borrarCamposInstrumentos();
                    refrescarInstrumentos();
                }
                break;

            /*case "Buscar por código":
                if (comprobarCodigoLimpio()) {
                    Util.showErrorAlert("Rellena el campo del código");
                    vista.tablaInstrumentos.clearSelection();
                } else {
                    try {
                        modelo.buscarInstrumentoPorCodigo(
                                vista.txtBuscarCodigo.getText());
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
                break;

            case "Buscar por nombre":
                if (comprobarNombreInstrumentoLimpio()) {
                    Util.showErrorAlert("Rellena el campo del nombre a buscar");
                    vista.tablaInstrumentos.clearSelection();
                } else {
                    try {
                        modelo.buscarInstrumentoPorNombre(
                                vista.txtBuscarNombre.getText()
                        );
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
                break;*/

            case "eliminarMaterialBtn":
                modelo.borrarMaterial(Integer.parseInt((String) vista.tablaMaterial.getValueAt(vista.tablaMaterial.getSelectedRow(), 0)));
                borrarCamposMaterial();
                refrescarMaterial();
                break;

            case "modificarMaterialBtn":
                if (vista.rbtnSi.isSelected()) {
                    vista.rbtnNo.setSelected(false);
                }
                if (vista.rbtnNo.isSelected()) {
                    vista.rbtnSi.setSelected(false);
                }

                if (comprobarMaterialLimpio()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    vista.tablaMaterial.clearSelection();
                } else {

                    modelo.modificarMaterial(
                            vista.txtNombreMaterial.getText(),
                            String.valueOf(vista.comboCalidad.getSelectedItem()),
                            vista.getOrganico(),
                            vista.txtWeb.getText(),
                            Integer.parseInt((String) vista.tablaMaterial.getValueAt(vista.tablaMaterial.getSelectedRow(), 0)));
                    refrescarMaterial();
                }
                borrarCamposMaterial();

                break;

            case "eliminarMarcaBtn":
                modelo.borrarMarca(Integer.parseInt((String) vista.tablaMarca.getValueAt(vista.tablaMarca.getSelectedRow(), 0)));
                borrarCamposMarca();
                refrescarMarca();
                break;

            case "modificarMarcaBtn":

                if (comprobarMarcaLimpio()) {
                    Util.showErrorAlert("Rellena todos los campos");
                    vista.tablaMarca.clearSelection();
                } else {
                    modelo.modificarMarca(
                            vista.txtNombreMarca.getText(),
                            String.valueOf(vista.comboPais.getSelectedItem()),
                            vista.txtDelegacion.getText(),
                            vista.txtWeb.getText(),
                            Integer.parseInt((String) vista.tablaMarca.getValueAt(vista.tablaMarca.getSelectedRow(), 0)));
                    refrescarMarca();
                }

                borrarCamposMarca();

                break;
        }

    }

    private void refrescarInstrumentos() {
        try {
            vista.tablaInstrumentos.setModel(construirTableModelInstrumentos(modelo.consultarInstrumento()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private TableModel construirTableModelInstrumentos(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmInstrumentos.setDataVector(data, columnNames);

        return vista.dtmInstrumentos;
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void refrescarMaterial() {
        try {
            vista.tablaMaterial.setModel(construirTableModelMaterial(modelo.consultarMaterial()));
            vista.comboMaterial.removeAllItems();
            for (int i = 0; i < vista.dtmMaterial.getRowCount(); i++) {
                vista.comboMaterial.addItem(vista.dtmMaterial.getValueAt(i, 0) + " - " +
                        vista.dtmMaterial.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private TableModel construirTableModelMaterial(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmMaterial.setDataVector(data, columnNames);
        return vista.dtmMaterial;
    }

    private void refrescarMarca() {
        try {
            vista.tablaMarca.setModel(construirTableModelMarca(modelo.consultarMarca()));
            vista.comboMarca.removeAllItems();
            for (int i = 0; i < vista.dtmMarca.getRowCount(); i++) {
                vista.comboMarca.addItem(vista.dtmMarca.getValueAt(i, 0) + " - " +
                        vista.dtmMarca.getValueAt(i, 2) + ", " + vista.dtmMarca.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private TableModel construirTableModelMarca(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmMarca.setDataVector(data, columnNames);

        return vista.dtmMarca;
    }

    private boolean comprobarInstrumentosLimpio() {
        return vista.txtCodigo.getText().isEmpty() ||
                vista.comboTipo.getSelectedIndex() == -1 ||
                vista.txtNombre.getText().isEmpty() ||
                vista.txtPvp.getText().isEmpty() ||
                vista.datePicker.getText().isEmpty() ||
                vista.comboMarca.getSelectedIndex() == -1 ||
                vista.comboMaterial.getSelectedIndex() == -1;
    }

    private boolean comprobarMarcaLimpio() {
        return vista.txtNombreMarca.getText().isEmpty() ||
                vista.comboPais.getSelectedIndex() == -1 ||
                vista.txtDelegacion.getText().isEmpty() ||
                vista.txtWeb.getText().isEmpty();

    }

    private boolean comprobarMaterialLimpio() {
        return vista.txtNombreMaterial.getText().isEmpty() ||
                vista.comboCalidad.getSelectedIndex() == -1 ||
                (!vista.rbtnNo.isSelected()&&!vista.rbtnSi.isSelected())||
                vista.txtProcedencia.getText().isEmpty();
    }

    private void borrarCamposInstrumentos() {
        vista.txtCodigo.setText("");
        vista.comboTipo.setSelectedIndex(-1);
        vista.txtNombre.setText("");
        vista.txtPvp.setText("");
        vista.datePicker.setDate(null);
        vista.comboMarca.setSelectedIndex(-1);
        vista.comboMaterial.setSelectedIndex(-1);

    }

    private void borrarCamposMarca() {
        vista.txtNombreMarca.setText("");
        vista.comboPais.setSelectedIndex(-1);
        vista.txtDelegacion.setText("");
        vista.txtWeb.setText("");
    }

    private void borrarCamposMaterial() {
        vista.txtNombreMaterial.setText("");
        vista.comboCalidad.setSelectedIndex(-1);
        vista.rbtnSi.setSelected(false);
        vista.rbtnNo.setSelected(false);
        vista.txtProcedencia.setText("");
    }

    boolean comprobarCodigoLimpio() {
        return vista.txtBuscarCodigo.getText().isEmpty();
    }

    boolean comprobarNombreInstrumentoLimpio() {
        return vista.txtBuscarNombre.getText().isEmpty();
    }

    private void setOptions() {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.confirmMessage("¿Desea cerrar la aplicación?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


}
