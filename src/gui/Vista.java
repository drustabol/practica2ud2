package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.Calidades;
import enums.Paises;
import enums.TipoInstrumentos;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;


public class Vista extends JFrame {
    private final static String titulo = "Instrumentos";
    private JPanel panel1;
    private String organico;

    JTabbedPane tabbedPane;
    JTextField txtCodigo;
    JTextField txtNombre;
    JTextField txtPvp;
    JTextField txtBuscarCodigo;
    JTextField txtBuscarNombre;
    JComboBox comboTipo;
    JComboBox comboMarca;
    JComboBox comboMaterial;

    private JPanel instrumentosPanel;
    JButton addInstrumentobtn;
    JButton eliminarInstrumentoBtn;
    JButton borrarTodosInstrumentosBtn;
    JTable tablaInstrumentos;
    DatePicker datePicker;
    //JButton buscarPorCódigoButton;
    //JButton buscarPorNombreButton;

    private JPanel marcaPanel;
    JTextField txtNombreMarca;
    JTextField txtDelegacion;
    JTextField txtWeb;
    JComboBox comboPais;
    JButton addMarcaBtn;
    JButton modificarMarcaBtn;
    JButton eliminarMarcaBtn;
    JTable tablaMarca;

    private JPanel materialPanel;
    JTextField txtNombreMaterial;
    JComboBox comboCalidad;
    JLabel lblSi;
    JLabel lblNo;
    JRadioButton rbtnSi;
    JRadioButton rbtnNo;
    JTextField txtProcedencia;
    JButton addMaterialBtn;
    JButton eliminarMaterialBtn;
    JButton modificarMaterialBtn;
    JButton modificarInstrumentosBtn;
    JTable tablaMaterial;
    private JScrollPane instrumentosScroll;
    private JScrollPane marcaScroll;
    private JScrollPane materialScroll;
    private JLabel lblCodigo;
    private JLabel lblNombre;
    private JLabel lblFecha;
    private JLabel lblMarca;
    private JLabel lblTipo;
    private JLabel lblPvp;
    private JLabel lblNombreMarca;
    private JLabel lblDelegacion;
    private JLabel lblPais;
    private JLabel lblWeb;
    private JLabel lblNombreMaterial;
    private JLabel lblOrganico;
    private JLabel lblCalidad;
    private JLabel lblProcedencia;
    private JLabel lblMaterial;

    DefaultTableModel dtmInstrumentos;
    DefaultTableModel dtmMarca;
    DefaultTableModel dtmMaterial;

    //MENU
    JMenuItem itemSalir;
    JMenuItem itemOpciones;
    JMenuItem itemParametros;

    //DIALOGO
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    Color color1,color2,color3,color4,color5,color6;

    Temas temas =new Temas();


    public Vista() {
        super(titulo);
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();

        paleta();
        this.setVisible(true);
        this.organico="";
        this.setLocationRelativeTo(this);
        optionDialog = new OptionDialog(this);
        crearMenu();
        setAdminDialog();
        setEnumCombo();
        setTableModels();
    }

    private void paleta() {
        color1=new Color(170,107,57);
        color2=new Color(128,68,21);
        color3=new Color(212,153,106);
        color4=new Color(255,255,255);
        color5=new Color(255,208,170);
        color6=Color.BLACK;
        setColors1(color1);
        setColor2(color2);
        setColor3(color3);
        setColor4(color4);
        setColor5(color5);
        setColor6(color6);
    }

    public void setColors1(Color color1) {
        panel1.setBackground(color1);
    }

    public void setColor2(Color color2) {
        tabbedPane.setBackground(color2);
    }

    public void setColor3(Color color3) {
        instrumentosPanel.setBackground(color3);
        marcaPanel.setBackground(color3);
        materialPanel.setBackground(color3);
        rbtnSi.setBackground(color3);
        rbtnNo.setBackground(color3);
    }

    public void setColor4(Color color4) {
        tabbedPane.setForeground(color4);

    }

    public void setColor5(Color color5) {
        comboTipo.setBackground(color5);
        comboMarca.setBackground(color5);
        comboMaterial.setBackground(color5);
        comboPais.setBackground(color5);
        comboCalidad.setBackground(color5);
        addMarcaBtn.setBackground(color5);
        addInstrumentobtn.setBackground(color5);
        addMaterialBtn.setBackground(color5);
        //buscarPorNombreButton.setBackground(color5);
        //buscarPorCódigoButton.setBackground(color5);
        eliminarInstrumentoBtn.setBackground(color5);
        eliminarMarcaBtn.setBackground(color5);
        eliminarMaterialBtn.setBackground(color5);
        borrarTodosInstrumentosBtn.setBackground(color5);
        modificarInstrumentosBtn.setBackground(color5);
        modificarMarcaBtn.setBackground(color5);
        modificarMaterialBtn.setBackground(color5);
    }
    public void setColor6(Color color6) {

        modificarMaterialBtn.setForeground(color6);
        addMarcaBtn.setForeground(color6);
        addInstrumentobtn.setForeground(color6);
        addMaterialBtn.setForeground(color6);
        eliminarInstrumentoBtn.setForeground(color6);
        eliminarMarcaBtn.setForeground(color6);
        eliminarMaterialBtn.setForeground(color6);
        borrarTodosInstrumentosBtn.setForeground(color6);
        modificarInstrumentosBtn.setForeground(color6);
        modificarMarcaBtn.setForeground(color6);
        lblCodigo.setForeground(color6);
        lblNombre.setForeground(color6);
        lblTipo.setForeground(color6);
        lblPvp.setForeground(color6);
        lblFecha.setForeground(color6);
        comboMaterial.setForeground(color6);
        comboMarca.setForeground(color6);
        comboCalidad.setForeground(color6);
        comboPais.setForeground(color6);
        comboTipo.setForeground(color6);
        lblNombreMarca.setForeground(color6);
        lblDelegacion.setForeground(color6);
        lblPais.setForeground(color6);
        lblWeb.setForeground(color6);
        lblNombreMaterial.setForeground(color6);
        lblOrganico.setForeground(color6);
        lblNo.setForeground(color6);
        lblSi.setForeground(color6);
        lblCalidad.setForeground(color6);
        lblProcedencia.setForeground(color6);
        lblMarca.setForeground(color6);
        lblMaterial.setForeground(color6);

    }



    public String getOrganico() {
        return organico;
    }

    public void setOrganico(String organico) {
        this.organico = organico;
    }

    private void crearMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu menuArchivo = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        itemParametros=new JMenuItem("Parámetros");
        itemParametros.setActionCommand("Parámetros");
        menuArchivo.add(itemParametros);
        menuArchivo.add(itemOpciones);
        menuArchivo.add(itemSalir);

        menuBar.add(menuArchivo);
        menuBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(menuBar);

    }

    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[]{adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña",
        JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);
        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

    private void setEnumCombo() {
        for (Calidades constant : Calidades.values()) {
            comboCalidad.addItem(constant.getValor());
        }
        comboCalidad.setSelectedIndex(-1);
        for (Paises constant : Paises.values()) {
            comboPais.addItem(constant.getValor());
        }
        comboPais.setSelectedIndex(-1);

        for (TipoInstrumentos constant : TipoInstrumentos.values()) {
            comboTipo.addItem(constant.getValor());
        }
        comboTipo.setSelectedItem(-1);
    }

    private void setTableModels() {
        this.dtmInstrumentos = new DefaultTableModel();
        this.tablaInstrumentos.setModel(dtmInstrumentos);
        this.dtmMarca = new DefaultTableModel();
        this.tablaMarca.setModel(dtmMarca);
        this.dtmMaterial = new DefaultTableModel();
        this.tablaMaterial.setModel(dtmMaterial);
    }
}
