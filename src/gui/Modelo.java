package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public Modelo() {
        getPropValues();
    }

    private Connection conexion;

    void getPropValues() {
        InputStream inputStream = null;


        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    void setPropValues(String ip, String user, String pass, String adminPass) {

        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://" + ip + ":3306/baseinstrumentos", user, password);
        } catch (SQLException e) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://" + ip + ":3306/", user, password);
                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();
            } catch (SQLException | IOException e0) {
                e0.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("instrumentos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    void insertarMaterial(String nombre, String calidad, String organico, String procedencia) {
        String sentenciaSql = "INSERT INTO material (nombreMaterial, calidad, organico, procedencia)" + "VALUES (?,?,?,?)";

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, calidad);
            sentencia.setString(3, String.valueOf(organico));
            sentencia.setString(4, procedencia);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void insertarMarca(String nombre, String pais, String delegacion, String web) {
        String sentenciaSql = "INSERT INTO marca (nombremarca, pais, delegacion, web)" + "VALUES (?,?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, pais);
            sentencia.setString(3, delegacion);
            sentencia.setString(4, web);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void insertarInstrumento(String codigo, String tipo, String nombre, double pvp, LocalDate fecha, String marca, String material) {
        String sentenciaSql = "INSERT INTO instrumentos (codigo, tipo, nombre, pvp, fecha_adquisicion, idmarca, idmaterial)" + "VALUES (?,?,?,?,?,?,?)";
        PreparedStatement sentencia = null;

        int idmarca = Integer.valueOf(marca.split(" ")[0]);
        int idmaterial = Integer.valueOf(material.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, codigo);
            sentencia.setString(2, tipo);
            sentencia.setString(3, nombre);
            sentencia.setDouble(4, pvp);
            sentencia.setDate(5, Date.valueOf(fecha));
            sentencia.setInt(6, idmarca);
            sentencia.setInt(7, idmaterial);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void modificarMaterial(String nombre, String calidad, String organico, String procedencia, int id) {
        String sentenciaSql = "UPDATE material SET nombreMaterial=?,calidad=?,organico=?,procedencia=?" + "WHERE id=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, calidad);
            sentencia.setString(3, organico);
            sentencia.setString(4, procedencia);
            sentencia.setInt(5,id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarMarca(String nombre, String pais, String delegacion, String web, int id) {
        String sentenciaSql = "UPDATE marca SET nombremarca=?,pais=?,delegacion=?,web=? WHERE id=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, pais);
            sentencia.setString(3, delegacion);
            sentencia.setString(4, web);
            sentencia.setInt(5,id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void modificarInstrumento(String codigo, String tipo, String nombre, double pvp, LocalDate fecha, String marca, String material) {
        String sentenciaSql = "UPDATE instrumentos SET codigo=?,tipo=?,nombre=?,pvp=?,fecha=?,idmarca=?,idmaterial=?" +
                "WHERE idInstrumento=?";
        PreparedStatement sentencia = null;
        int idmarca = Integer.valueOf(marca.split(" ")[0]);
        int idmaterial = Integer.valueOf(material.split(" ")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, codigo);
            sentencia.setString(2, tipo);
            sentencia.setString(3, nombre);
            sentencia.setDouble(4, pvp);
            sentencia.setDate(5, Date.valueOf(fecha));
            sentencia.setInt(6, idmarca);
            sentencia.setInt(7, idmaterial);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void borrarMaterial(int id) {
        String sentenciaSql = "DELETE FROM material WHERE id=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void borrarMarca(int id) {
        String sentenciaSql = "DELETE FROM marca WHERE id=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void borrarInstrumento(int idInstrumento) {
        String sentenciaSql = "DELETE FROM instrumentos WHERE idinstrumento=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idInstrumento);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void killemall() {
        String sentenciaSql = "DELETE FROM instrumentos;";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    ResultSet consultarMaterial() throws SQLException {
        String sentenciaSql = "SELECT concat(id) as 'ID', concat(nombreMaterial) as 'Nombre material', " +
                "concat(calidad) as 'calidad', concat(organico) as 'Orgánico',concat(procedencia) as 'Procedencia'" +
                "FROM material";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarMarca() throws SQLException {
        String sentenciaSql = "SELECT concat(id) as 'ID', concat(nombreMarca) as 'Nombre marca', " +
                "concat(pais) as 'País', concat(delegacion) as 'Delegación', concat(web) as 'Web'" +
                "FROM marca";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarInstrumento() throws SQLException {
        String sentenciaSql = "SELECT concat(i.idinstrumento) as 'ID', concat(i.codigo) as 'Código', concat(i.tipo) as 'Tipo',\n" +
                "concat(i.nombre) as 'Nombre', concat(i.pvp) as 'Pvp', concat(i.fecha_adquisicion) as 'Fecha adquisición',\n" +
                "concat(m.id,' : ',m.nombremarca) as 'Marca', concat(b.id,' : ',b.nombrematerial) as 'Material'\n" +
                "FROM instrumentos as i inner join marca as m on m.id=i.idmarca inner join\n" +
                "material as b on b.id = i.idmaterial";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    public boolean materialYaExiste(String nombre) {
        String consulta = "SELECT existeMaterial(?)";
        PreparedStatement function;
        boolean materialExists = false;
        try {
            function = conexion.prepareStatement(consulta);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();
            materialExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return materialExists;
    }

    public boolean marcaYaExiste(String nombre) {
        String consulta = "SELECT existeMarca(?)";
        PreparedStatement function;
        boolean marcaExists = false;
        try {
            function = conexion.prepareStatement(consulta);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();
            marcaExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return marcaExists;
    }

    public boolean codigoYaExiste(String codigo) {
        String consulta = "SELECT existeCodigo(?)";
        PreparedStatement function;
        boolean codigoExists = false;
        try {
            function = conexion.prepareStatement(consulta);
            function.setString(1, codigo);
            ResultSet rs = function.executeQuery();
            rs.next();
            codigoExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigoExists;
    }

    public boolean instrumentoYaExiste(String nombre) {
        String consulta = "SELECT existeCodigo(?)";
        PreparedStatement function;
        boolean instrumentExists = false;
        try {
            function = conexion.prepareStatement(consulta);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();
            instrumentExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return instrumentExists;
    }

    /*ResultSet buscarInstrumentoPorCodigo(String codigo) throws SQLException {
        String sentenciaSql = "SELECT concat(i.idinstrumento) as 'ID', concat(i.codigo) as 'Código', concat(i.tipo) as 'Tipo',\n" +
                "concat(i.nombre) as 'Nombre', concat(i.pvp) as 'Pvp', concat(i.fecha_adquisicion) as 'Fecha adquisición',\n" +
                "concat(m.id,' : ',m.nombremarca) as 'Marca', concat(b.id,' : ',b.nombrematerial) as 'Material'\n" +
                "FROM instrumentos as i inner join marca as m on m.id=i.idmarca inner join\n" +
                "material as b on b.id = i.idmaterial WHERE codigo=?" ;
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;

        String sentenciaSql="SELECT * FROM instrumentos WHERE codigo=?";
        PreparedStatement sentencia=null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado=null;
        sentencia.setString(1,codigo);
        resultado=sentencia.executeQuery();

        return resultado;
    }

    ResultSet buscarInstrumentoPorNombre(String nombre) throws SQLException {
        String sentenciaSql="SELECT * FROM instrumentos WHERE nombre=?";
        PreparedStatement sentencia=null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado=null;
        sentencia.setString(1,nombre);

        return resultado;
    }*/
}
